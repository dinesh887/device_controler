var express = require('express');
var http = require("http");
var cron = require('node-cron');
var socketio = require('socket.io');
var bodyParser = require('body-parser');
var socket = require('./socket/socket-handle');
var app = express();
var server = http.createServer(app);
var io = socketio.listen(server);
socket.HandleSocket(io);
var SerialPort = require('serialport');
var parsers = SerialPort.parsers;
var parser = new parsers.Readline({
    delimiter: '\r\n'
});
app.set('port', 3000);
app.set('ip', '0.0.0.0');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));


var trinamic = require('./devices/trinamic');
var moxa = require('./devices/moxa');
var recipes = require('./devices/recipes');
var Control = require('./StateMachines/MAIN');
Control.Start(moxa,trinamic);

cron.schedule('*/1 * * * * *', function () {
    //console.log('running a task every 1 sec');
    try {
        socket.EmitUpdateIosForAllUsers(moxa.GetIos());
        socket.EmitUpdateStateMachineForAllUsers(Control.GetState());
    } catch (e) {
        console.log(e);
    }
});

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.post('/SetCmd', function (req, res) {
    var resp = Control.SetCmd(req.body);
    res.send(resp);
});

app.get('/GetMotorInfos', function (req, res) {
    res.send(trinamic.GetInfos());
});


app.post('/getStateMachineController', function (req, res) {
    var resp = Control.GetState();
    res.send(resp);
});

app.get('/getIos', function (req, res) {
    moxa.GetIos(function (err, data) {
        if (err) {
            res.send(err);
        } else {
            res.send(data);
        }
    });
});

app.post('/setIos', function (req, res) {
    moxa.SetIos(req.body, function (err, data) {
        if (err) {
            res.send(JSON.stringify(err));
        } else {
            res.send(data);
        }
    });
});
app.get('/getRecipes', function (req, res) {
    recipes.GetRecipes(1, function (err, data) {
        if (err) {
            res.send(err);
        } else {
            res.send(data);
        }
    });
});
app.get('/getRecipeVr', function (req, res) {
    recipes.GetRecipes(2, function (err, data) {
        if (err) {
            res.send(err);
        } else {
            res.send(data);
        }
    });
});
app.post('/updateRecipes', function (req, res) {
    recipes.UpdateRecipes(req, 1, function (err, data) {
        if (err) {
            res.send(err);
        } else {
            res.send(data);
        }

    });
});
app.post('/updateRecipesVr', function (req, res) {
    recipes.UpdateRecipes(req, 2, function (err, data) {
        if (err) {
            res.send(err);
        } else {
            res.send(data);
        }

    });
});
server.listen(app.get('port'), function () {
    console.log('*** server started ***: ');
});