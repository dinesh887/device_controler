var fs = require('fs');

var getRecipes = function (file,next) {
    var recipesFile = '';
    if(file ===1 ){
        recipesFile = __dirname + "/config/recipes.json";
    }else{
        recipesFile = __dirname + "/config/recipe_vr.json";
    }

    fs.readFile(recipesFile, (err, data) => {
        if (err) {
            next(err, null);
        } else {
            next(null, data);
        }
    });
};
var updateRecipes = function (req,fileType,next) {
    var params = req.body;
    var recipesFile = '';
     if(fileType === 1){
        recipesFile =__dirname + "/config/recipes.json";
     }else{
         recipesFile =__dirname + "/config/recipe_vr.json";
     }
    fs.writeFile(recipesFile, params.requestPrams,(err) => {
        if (err) {
            next(err, null);
        } else {
            var success = {
                code: 1
            }
            next(null, success);
        }
    });
};

module.exports.GetRecipes = getRecipes;
module.exports.UpdateRecipes = updateRecipes;