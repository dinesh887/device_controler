/* global __dirname */

var fs = require('fs');
var request = require('request');
var http = require('http');
var NanoTimer = require('nanotimer');
var async = require('async');
var serverIp;
var serverpath;
var serverUrl = {};
var m_Ios = {};

function LoadConfigurations() {
    var configFileLocation = __dirname + "/config/moxaConfig.json";
    var data = fs.readFileSync(configFileLocation);
    try {
        data = JSON.parse(data);
        serverIp = data.SERVER_IP;
        serverUrl.do = data.PATHDO;
        serverUrl.di = data.PATHDI;

    } catch (e) {
        console.log(e);
    }
}

function WriteToMoxa(method, requestData, next) {
    var options = {
        hostname: serverIp,
        path: encodeURI(serverpath),
        method: method,
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'vdn.dac.v1',
            'Content-Length': Buffer.byteLength(JSON.stringify(requestData))
        }
    };
    var responseData = '';
    var request = http.request(options, function (response) {
        response.setEncoding('utf-8');
        response.on('data', function (data) {
            responseData += data;
        });
        response.on('end', function (data) {
            console.log("Response  : " + responseData);
            try {
                if (method === 'GET') {
                    next(null, JSON.parse(responseData));
                } else {
                    next(responseData, null);
                }

            } catch (ex) {
                console.log(ex);
            }
        });
    });
    request.on('error', function (e) {
        console.log('problem with request: ' + e.message);
        next(e.message, null);
    });
    try {
        request.write(JSON.stringify(requestData));
        request.end();
    } catch (reqEx) {
        console.log("Write moxa exception: " + reqEx.message);
        next(reqEx.message, null);
    }
}

function ReadFromMoxa() {
    var options = {
        method: "GET",
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'vdn.dac.v1',
            'Content-Length': Buffer.byteLength(JSON.stringify(""))
        }
    };
    async.parallel({
        dos: function () {
            options.uri = 'http://' + serverIp + serverUrl.do;
            request(options, function (error, response, body) {
                if (!error && response.statusCode === 200) {
                    response.setEncoding('utf-8');
                    m_Ios.do = JSON.parse(body);

                } else {
                    console.log(JSON.stringify(error));
                }
            });
        },
        dis: function () {
            options.uri = 'http://' + serverIp + serverUrl.di;
            request(options, function (error, response, body) {
                if (!error && response.statusCode === 200) {
                    response.setEncoding('utf-8');
                    m_Ios.di = JSON.parse(body);

                } else {
                    console.log(JSON.stringify(error));
                }
            });
        }, function() {

        }
    });
}

var getIos = function () {
    return m_Ios;
};
var setIos = function (req, next) {
    var params = req;
    console.log(params);
    WriteToMoxa("PUT", params, function (err, data) {
        if (!err) {
            next(null, data);
        } else {
            var message = {'error': err};
            next(message, null);
        }
    });
};

LoadConfigurations();
var timerObject = new NanoTimer();
timerObject.setInterval(ReadFromMoxa, '', '200m');

module.exports.GetIos = getIos;
module.exports.SetIos = setIos;