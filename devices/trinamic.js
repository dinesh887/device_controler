/* global __dirname */

var fs = require('fs');
var SerialPort = require('serialport');
var NanoTimer = require('nanotimer');
var parsers = SerialPort.parsers;
var parser = new parsers.Readline({
    delimiter: '\r\n'
});

var m_Cmds = null;
var m_Motors = null;
var m_Speed = null;
var m_SerialPort = null;
var m_CmdQueue = [];
var m_CmdOld = "";
var m_checkForPort = false;
var timerSetPort = new NanoTimer();
var timerWatchdog = new NanoTimer();
var m_Watchdog = 0;
var m_WatchdogOld = -1;
var m_WatchdogAlarm = false;
var m_Homed = false;
var m_positionReached = false;
var m_port = null;

function CheckWatchdog() {
    m_WatchdogAlarm = m_Watchdog === m_WatchdogOld;
    if (m_WatchdogAlarm) {
        console.log("Watchdog Alarm");
    }
}

function LoadConfig() {
    var configFileLocation = __dirname + "/config/trinamicConfig.json";
    var data = fs.readFileSync(configFileLocation);
    try {
        m_Sim = JSON.parse(data).Simulation;
        m_Speed = JSON.parse(data).Speed;
        m_Cmds = JSON.parse(data).Commands;
        m_Motors = JSON.parse(data).Motors;
        m_CmdQueue.push(m_Cmds.getPosition);
        InitMotors();
        if (!m_Sim) {
            SetPort();
        } else {
            console.log("Trinamic simulation is on");
            PortSimulation();
        }
    } catch (e) {
        console.log(e);
    }
}

function SetPort() {
    SerialPort.list(function (err, results) {
        if (err) {
            console.log(err.message);
        } else {
            console.log(JSON.stringify(results));
            for (var com in results) {
                if (results[com].vendorId && results[com].vendorId === '16D0') {
                    var port = results[com].comName;
                    m_port = results[com];
                    console.log("Trinamic port: " + JSON.stringify(port));
                    m_SerialPort = new SerialPort(port, function (err) {
                        if (err) {
                            console.log('Error: ', err.message);
                            checkPort();
                        } else {
                            console.log('Port open');
                            m_checkForPort = false;
                            timerSetPort.clearInterval();
                            timerWatchdog.setInterval(CheckWatchdog, '', '1s');
                            m_SerialPort.on('data', function (data) {
                                var sendData = data.toJSON().data;
                                resp = {};
                                resp.value = (sendData[4] << 24) | (sendData[5] << 16) | (sendData[6] << 8) | sendData[7];
                                resp.status = sendData[2] === 100 ? 'OK' : 'Failed';
                                resp.address = sendData[0];
                                if (resp.status === 'Faild') {
                                    console.log(resp);
                                }
                                //setTimeout(Received, 1000, resp);
                                Received(resp);
                                m_Watchdog = (m_Watchdog > 32000) ? 0 : m_Watchdog + 1;
                            });
                            sendCommand();
                        }
                    });
                    m_SerialPort.pipe(parser);
                    return;
                }
            }

            checkPort();
        }
    });
}

function PortSimulation() {
    resp = {};
    if (m_CmdQueue.length > 0) {
        switch (m_CmdQueue[0].name) {
            case m_Cmds.homingState.name:
                resp.value = !resp.value;
                break;
            case m_Cmds.getPosition.name:
                resp.value = m_CmdQueue[0].value;
                break;
            case m_Cmds.setHoming.name:

            case m_Cmds.positionReached.name:
                break;
        }
        resp.status = 'OK';
    }
    setTimeout(Received, 200, resp);
}

function Received(res) {
    var skip = false;
    if (m_CmdQueue.length > 0) {
        switch (m_CmdQueue[0].name) {
            case m_Cmds.homingState.name:
                if (res.value === 0) {
                    m_CmdQueue.splice(0, 1);
                }
                skip = true;
                break;
            case m_Cmds.getPosition.name:
                m_Motors[m_CmdQueue[0].motor].actPosition = res.value;
                var cmd = JSON.parse(JSON.stringify(m_CmdQueue[0]));
                cmd.motor = (cmd.motor === m_Motors.length - 1) ? 0 : cmd.motor + 1;
                m_CmdQueue.push(cmd);
                break;
            case m_Cmds.setHoming.name:
                m_Homed = true;
                break;
            case m_Cmds.positionReached.name:
                if (res.value === 1) {
                    m_positionReached = true;
                    m_CmdQueue.splice(0, 1);
                }
                skip = true;
                break;
        }

        if (!skip) {
            m_CmdQueue.splice(0, 1);
        }
        if (m_CmdQueue.length === 0) {
            m_CmdQueue.push(m_Cmds.getPosition);
        }

        sendCommand();
    }
}

function InitMotors() {
    for (var motor in m_Motors) {
        m_Motors[motor].actPosition = -1;
    }
}

function GenerateMessage(cmd) {
    var params = cmd;
    var Value = (typeof params.value === 'undefined')? 0 :parseInt(params.value);
    var write_buffer = [9];
    write_buffer[0] = params.address;
    write_buffer[1] = params.command;
    write_buffer[2] = params.type;
    write_buffer[3] = parseInt(params.motor);
    write_buffer[4] = Value >> 24;
    write_buffer[5] = Value >> 16;
    write_buffer[6] = Value >> 8;
    write_buffer[7] = Value & 0xff;
    write_buffer[8] = 0;
    for (i = 0; i < 8; i++)
        write_buffer[8] += write_buffer[i];
    return write_buffer;
}

var setCommand = function (cmd) {
    m_CmdQueue.push(cmd);
};

var sendCommand = function () {
    if (!m_WatchdogAlarm) {
        if (m_CmdQueue[0]) {
            if (m_CmdOld !== m_CmdQueue[0].name) {
                console.log("sendCommend" + JSON.stringify(m_CmdQueue[0]));
                m_CmdOld = m_CmdQueue[0].name;
            }
            var cmd = m_CmdQueue[0];
            var message = GenerateMessage(cmd);
            if (m_Sim) {
                PortSimulation();
            } else {
                m_SerialPort.write(message, function (err) {
                    if (err) {
                        console.log('Error on write: ', err.message);
                        checkPort();
                    }
                    //console.log(JSON.stringify(message) + ' message written');
                });
            }
        }
    }
};

var checkPort = function () {
    if (!m_checkForPort) {
        m_checkForPort = true;
        timerSetPort.setInterval(SetPort, '', '5s');
    }
};

var homing = function () {
    m_Homed = false;
    m_CmdQueue.push(m_Cmds.resetHoming);
    for (var motor in m_Motors) {
        var cmd = JSON.parse(JSON.stringify(m_Cmds.homing));
        cmd.motor = motor;
        m_CmdQueue.push(cmd);
        cmd2 = JSON.parse(JSON.stringify(m_Cmds.homingState));
        cmd2.motor = motor;
        m_CmdQueue.push(cmd2);
    }
    m_CmdQueue.push(m_Cmds.setHoming);
};

var allToPositionZero = function () {
    for (var motor in m_Motors) {
        var cmd = JSON.parse(JSON.stringify(m_Cmds.moveToPosition));
        cmd.motor = motor;
        cmd.value = 0;
        m_CmdQueue.push(cmd);
        m_CmdQueue.push(m_Cmds.positionReached);
    }
    console.log(m_CmdQueue);
};

var goToPosition = function (position, motor) {
    m_positionReached = false;
    var motorNumber = GetMotorByName(motor);
    m_Cmds.moveToPosition.motor = motorNumber;
    m_Cmds.moveToPosition.value = position;
    m_CmdQueue.push(m_Cmds.moveToPosition);
    m_CmdQueue.push(m_Cmds.positionReached);
};

var standby = function () {
    for (var motor in m_Motors) {
        if (m_Motors[motor].actPosition !== 0) {
            return false;
        }
    }
    return true;
};

var homed = function () {
    return m_Homed;
};

var positionReached = function () {
    return m_positionReached;
};

function GetMotorByName(motorName) {
    for (var motor in m_Motors) {
        if (m_Motors[motor].Name === motorName) {
            return motor;
        }
    }
    console.log("Motor existiert nicht!!");
}

var getInfos = function () {
    var obj = {};
    var port = {};
    port.comName = "not Available";
    obj.port = (m_port !== null) ? m_port : port;
    obj.motors = m_Motors;
    obj.cmds = Object.keys(m_Cmds);
    return obj;
};

var getCmd = function (cmdName) {
    return m_Cmds[cmdName];
};

var getMotors = function () {
    return m_Motors;
};

var getMotorByName = function (name) {
    for (var motor in m_Motors) {
        if (m_Motors[motor].Name === name) {
            return motor;
        }
    }
};

var getSpeed = function (name) {
    return m_Speed[name];
};

LoadConfig();

module.exports.SetCommand = setCommand;
module.exports.Homing = homing;
module.exports.Homed = homed;
module.exports.Standby = standby;
module.exports.AllToPositionZero = allToPositionZero;
module.exports.GoToPosition = goToPosition;
module.exports.PositionReached = positionReached;
module.exports.GetInfos = getInfos;
module.exports.GetCmd = getCmd;
module.exports.GetMotors = getMotors;
module.exports.GetMotor = getMotorByName;
module.exports.GetSpeed = getSpeed;